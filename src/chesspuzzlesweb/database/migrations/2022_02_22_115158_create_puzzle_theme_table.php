<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePuzzleThemeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puzzle_theme', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("puzzle_id");
            $table->unsignedBigInteger("theme_id");
            $table->timestamps();

            $table->foreign("puzzle_id")->references("id")->on("puzzles")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("theme_id")->references("id")->on("themes")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puzzle_theme');
    }
}
