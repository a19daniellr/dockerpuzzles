<?php

namespace Database\Seeders;

use App\Models\PuzzleTheme;
use App\Models\Solved;
use App\Models\Stat;
use App\Models\Theme;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(20)->create();
        Theme::factory(20)->create();
        Stat::factory(15)->create();
        PuzzleTheme::factory(100)->create();
        Solved::factory(3)->create();
    }
}
