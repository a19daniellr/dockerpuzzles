<?php

namespace Database\Factories;

use App\Models\Stat;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatFactory extends Factory
{
    protected $model = Stat::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::all()->count();

        return [
            "user_id" => $this->faker->unique()->numberBetween(1, $users),
            "score" => $this->faker->numberBetween(500, 2800),
        ];
    }
}
