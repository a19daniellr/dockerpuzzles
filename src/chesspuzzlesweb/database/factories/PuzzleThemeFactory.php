<?php

namespace Database\Factories;

use App\Models\Puzzle;
use App\Models\PuzzleTheme;
use App\Models\Theme;
use Illuminate\Database\Eloquent\Factories\Factory;

class PuzzleThemeFactory extends Factory
{
    protected $model = PuzzleTheme::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $puzzles = Puzzle::all()->count();
        $themes = Theme::all()->count();

        return [
            "puzzle_id" => $this->faker->numberBetween(1, $puzzles),
            "theme_id" => $this->faker->numberBetween(1, $themes),
        ];
    }
}
