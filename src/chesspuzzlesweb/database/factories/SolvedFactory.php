<?php

namespace Database\Factories;

use App\Models\Puzzle;
use App\Models\Solved;
use App\Models\Stat;
use Illuminate\Database\Eloquent\Factories\Factory;

class SolvedFactory extends Factory
{
    protected $model = Solved::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $puzzles = Puzzle::all()->count();
        $stats = Stat::all()->count();
        return [
            "puzzle_id" => $this->faker->unique()->numberBetween(1, $puzzles),
            "stats_id" => $this->faker->unique()->numberBetween(1, $stats),
        ];
    }
}
