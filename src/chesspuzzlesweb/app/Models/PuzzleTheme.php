<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class PuzzleTheme extends Pivot
{
    use HasFactory;

    protected $fillable = array("theme_id", "puzzle_id");
}
