<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Puzzle extends Model
{
    use HasFactory;

    protected $fillable = array("fen", "moves", "rating", "web", "popularity");

    public function stats()
    {
        return $this->belongsToMany(Stats::class);
    }

    public function themes()
    {
        return $this->belongsToMany(Themes::class);
    }
}
