<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    use HasFactory;

    protected $fillable = array("user_id", "score");

    public function users()
    {
        return $this->hasOne(User::class, "user_id");
    }

    public function puzzles()
    {
        return $this->belongsToMany(Puzzles::class)->withTimestamps();
    }
}
