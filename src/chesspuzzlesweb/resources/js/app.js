require("./bootstrap");

import Alpine from "alpinejs";
import Vue from "vue";
import Chessboard from "./components/Chessboard2.vue";

window.Alpine = Alpine;

Alpine.start();

const app = new Vue({
    name: "app",
    components: {
        chessboardtest: Chessboard,
    },
}).$mount("#app");
