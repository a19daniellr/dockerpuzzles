# Proxecto final de DAPW 2DAW de Daniel Lago Riomao
Este proxecto está feito cun video explicativo, o video explicativo está feito con son. Nese vídeo explico os conceptos de todo o proxecto: problemas, solucións, Dockerfile...
## Contidos
### SRC
Contén:
- O contido completo do proxecto
- Os diferentes arquivos ".env" dependendo do entorno de traballo (desenrrolo ou producción).
### PHP
Contén: 
- O Dockerfile que define o contido da imaxen para o servizo de PHP.
- O arquivo "www.conf" que se sobreescribe para que PHP escoite no porto 9000 en vez de nun arquivo.
### NGINX
- Os certificados da aplicación WEB.
- O "server.conf" da aplicación WEB. Este arquivo define o root do noso proxecto, o server_name, etc.
### DB
- O arquivo ".my.cnf", que define os parámetros de login para a base de datos en desenrrolo.
- Os scripts para manter a Base de datos actualizada en todo momento.
- A base de datos en sí para poder tela gardade e subila ao servizo ao arrancalo.
### ROOT
- Os docker-files para os diferentes entornos de traballo.
- O .env para as variables da base de datos.
## Puntos a ter en conta
Se se quere clonar este repositorio sempre recordar en dar permisos 775 as carpetas "/storage" e "/bootstrap/cache" do proxecto laravel. Ademáis de asegurarse de que os scripts teñen permisos de execución.
